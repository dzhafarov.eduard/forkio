const {src, dest} = require("gulp"),
    uglify = require('gulp-uglify'),
    jsMinify = require('gulp-js-minify'),
    concat = require('gulp-concat'),
    browserSync = require("browser-sync");




const scripts = () =>{
    return src("./src/js/*.js")
        .pipe(uglify())
        .pipe(concat('scripts.min.js'))
        .pipe(jsMinify())
        .pipe(dest("./dist/js"))
        .pipe(browserSync.stream());
}

exports.scripts = scripts;
