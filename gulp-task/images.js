const gulp = require("gulp");
const {src, dest} = require("gulp");
const webp = require("gulp-webp");
const imagemin  = require("gulp-imagemin");


const images = () =>{
    return src("./src/images/**/*.{png, jpg, jpeg, gif, svg}")
        .pipe(webp({quality: 70,}))
        .pipe(imagemin({
                interlaced: true,
                progressive: true,
                optimizationLevel: 3,
                svgoPlugins: [{ removeViewBox: false }],
            }))
        .pipe(dest("./dist/images/"))

}

exports.images = images;