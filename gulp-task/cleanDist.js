const {src, dest} = require("gulp");
const  clean = require("gulp-clean") ;


const cleanDist = () =>{
    return src('./dist/**', {read: false})
        .pipe(clean())

        .pipe(dest('./dist'));
}

exports.cleanDist = cleanDist;