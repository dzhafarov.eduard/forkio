const {series, parallel} = require("gulp");
const{style} = require("./gulp-task/style.js");
const{serv} = require("./gulp-task/serv.js");
const{scripts} = require("./gulp-task/script.js");
const{images} = require("./gulp-task/images.js")
const{watch} = require("./gulp-task/watch");
const {cleanDist} = require("./gulp-task/cleanDist");


const build = () =>{
    return series(cleanDist, style, scripts, images)
}

const dev = (cb) => {
    return parallel(serv, watch)
    (cb);

}

exports.default = images;
exports.default = parallel(dev, build);